<?php

    namespace App\Controllers;

    class LaptopController extends \App\Core\Controller {
        public function show($id) {
            $laptopModel = new \App\Models\LaptopModel($this->getDatabaseConnection());
            $laptop = $laptopModel->getById($id);
          
            if (!$laptop) {
                header('Location: /');
                exit;
            }

            $this->set('laptop', $laptop);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($laptop->category_id);

            if (!$category) {
                header('Location: /');
                exit;
            }

            $this->set('category', $category);

            
        }

      
    }