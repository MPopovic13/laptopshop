<?php
    namespace App\Controllers;

    class CategoryController extends \App\Core\Controller {
        public function show($id) {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if (!$category) {
                header('Location: /');
                exit;
            }

            $this->set('category', $category);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            $laptopModel = new \App\Models\LaptopModel($this->getDatabaseConnection());
            $laptops = $laptopModel->getAllByFieldName("category_id",$id);
            $this->set('laptops', $laptops);
            
          
        }
    }
