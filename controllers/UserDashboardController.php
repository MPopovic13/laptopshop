<?php
    namespace App\Controllers;

    class UserDashboardController extends \App\Core\Role\UserRoleController {
        public function index() {
            $userId = $this->getSession()->get('user_id');
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($userId);
            $this->set('user', $user);

        }
    }
