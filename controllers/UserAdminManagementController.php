<?php
    namespace App\Controllers;

    class UserAdminManagementController extends \App\Core\Role\UserRoleController {
        public function admin() {
         

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $users = $userModel->getAll();

            $this->set('users', $users);
        }

        public function getAdd() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function postAdd() {
            


            $addData = [
                'username'               => \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING),          
                'password'               => \filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING),
                'email'                  => \filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING),
                'firstname'              => \filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING),              
                'lastname'               => \filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING),   
            ];

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            \print_r($addData);


            $addData['password'] = \password_hash($addData['password'], PASSWORD_DEFAULT);


            $userId = $userModel->add($addData);

            if (!$userId) {
                $this->set('message', 'Nije dodat admin.');
                return;
            }

            

            $this->redirect( \Configuration::BASE . 'user/admin' );
        }

        public function getEdit($userId) {
            $userModel = new \App\Models\userModel($this->getDatabaseConnection());
            $user = $userModel->getById($userId);

            if (!$user) {
                $this->redirect( \Configuration::BASE . 'user/admin' );
                return;
            }

            if ($user->user_id != $this->getSession()->get('user_id')) {
                $this->redirect( \Configuration::BASE . 'user/admin' );
                return;
            }


            $this->set('user', $user);

           
        }

        public function postEdit($userId) {
            $this->getEdit($userId);

            $editData = [
                'username'               => \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING),          
                'password'               => \filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING),
                'email'                  => \filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING),
                'firstname'              => \filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING),              
                'lastname'               => \filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING),   
            ];

            $userModel = new \App\Models\userModel($this->getDatabaseConnection());

            $editData['password'] = \password_hash($editData['password'], PASSWORD_DEFAULT);

            $res = $userModel->editById($userId, $editData);
            if (!$res) {
                $this->set('message', 'Nije bilo moguce izmeniti admina.');
                return;
            }

            $this->redirect( \Configuration::BASE . 'user/admin' );
        }
        
    }
