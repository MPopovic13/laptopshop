<?php
    namespace App\Controllers;

    class UserLaptopManagementController extends \App\Core\Role\UserRoleController {
        public function laptop() {
           // $userId = $this->getSession()->get('user_id');

            $laptopModel = new \App\Models\LaptopModel($this->getDatabaseConnection());
            $laptops = $laptopModel->getAll();

            $this->set('laptops', $laptops);
        }

        public function getAdd() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function getDelete($laptopId){
            $laptopModel = new \App\Models\LaptopModel($this->getDatabaseConnection());
            $laptopModel->deleteById($laptopId);
            $this->redirect( \Configuration::BASE . 'user/laptop' );

        }

        public function postAdd() {
            $addData = [
                'title'                  => \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING),
                'price'                  => sprintf("%.2f", \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING)),
                'category_id'            => \filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT),               
                'os'                     => \filter_input(INPUT_POST, 'os', FILTER_SANITIZE_STRING),
                'disc_capacity'          => \filter_input(INPUT_POST, 'disc_capacity', FILTER_SANITIZE_NUMBER_INT),
                'disc_type'              => \filter_input(INPUT_POST, 'disc_type', FILTER_SANITIZE_STRING),
                'diagonal'               => sprintf("%.2f", \filter_input(INPUT_POST, 'diagonal', FILTER_SANITIZE_STRING)),
                'keyboard'               => \filter_input(INPUT_POST, 'keyboard', FILTER_SANITIZE_STRING),
                'numpad'                 => \filter_input(INPUT_POST, 'numpad', FILTER_SANITIZE_NUMBER_INT),
                'connector_type'         => \filter_input(INPUT_POST, 'connector_type', FILTER_SANITIZE_STRING),
                'processor'              => \filter_input(INPUT_POST, 'processor', FILTER_SANITIZE_STRING),
                'graphic_card_type'      => \filter_input(INPUT_POST, 'graphic_card_type', FILTER_SANITIZE_STRING),
                'graphic_card'           => \filter_input(INPUT_POST, 'graphic_card_type', FILTER_SANITIZE_STRING),
                'ram'                    => \filter_input(INPUT_POST, 'ram', FILTER_SANITIZE_NUMBER_INT)
                
            ];

            $laptopModel = new \App\Models\laptopModel($this->getDatabaseConnection());

            $laptopId = $laptopModel->add($addData);

            if (!$laptopId) {
                $this->set('message', 'Nije dodat laptop.');
                return;
            }

            $uploadStatus = $this->doImageUpload('image', $laptopId);
            if (!$uploadStatus) {
                return;
            }

            $this->redirect( \Configuration::BASE . 'user/laptop' );
        }

        public function getEdit($laptopId) {
            $laptopModel = new \App\Models\laptopModel($this->getDatabaseConnection());
            $laptop = $laptopModel->getById($laptopId);

            if (!$laptop) {
                $this->redirect( \Configuration::BASE . 'user/laptop' );
                return;
            }
/*
            if ($laptop->user_id != $this->getSession()->get('user_id')) {
                $this->redirect( \Configuration::BASE . 'user/laptops' );
                return;
            }

     
*/
           

            $this->set('laptop', $laptop);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function postEdit($laptopId) {
            $this->getEdit($laptopId);

            $editData = [
                'title'                  => \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING),
                'price'                  => sprintf("%.2f", \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING)),
                'category_id'            => \filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT),               
                'os'                     => \filter_input(INPUT_POST, 'os', FILTER_SANITIZE_STRING),
                'disc_capacity'          => \filter_input(INPUT_POST, 'disc_capacity', FILTER_SANITIZE_NUMBER_INT),
                'disc_type'              => \filter_input(INPUT_POST, 'disc_type', FILTER_SANITIZE_STRING),
                'diagonal'               => sprintf("%.2f", \filter_input(INPUT_POST, 'diagonal', FILTER_SANITIZE_STRING)),
                'keyboard'               => \filter_input(INPUT_POST, 'keyboard', FILTER_SANITIZE_STRING),
                'numpad'                 => \filter_input(INPUT_POST, 'numpad', FILTER_SANITIZE_NUMBER_INT),
                'connector_type'         => \filter_input(INPUT_POST, 'connector_type', FILTER_SANITIZE_STRING),
                'processor'              => \filter_input(INPUT_POST, 'processor', FILTER_SANITIZE_STRING),
                'graphic_card_type'      => \filter_input(INPUT_POST, 'graphic_card_type', FILTER_SANITIZE_STRING),
                'graphic_card'           => \filter_input(INPUT_POST, 'graphic_card', FILTER_SANITIZE_STRING),
                'ram'                    => \filter_input(INPUT_POST, 'ram', FILTER_SANITIZE_NUMBER_INT)    
            ];

            $laptopModel = new \App\Models\laptopModel($this->getDatabaseConnection());

            $res = $laptopModel->editById($laptopId, $editData);
            if (!$res) {
                $this->set('message', 'Nije bilo moguce izmeniti laptop.');
                return;
            }

            if (isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
                $uploadStatus = $this->doImageUpload('image', $laptopId);
                if (!$uploadStatus) {
                    return;
                }
            }

            $this->redirect( \Configuration::BASE . 'user/laptop' );
        }

        private function doImageUpload(string $fieldName, string $laptopId): bool {
            $laptopModel = new \App\Models\laptopModel($this->getDatabaseConnection());
            $laptop = $laptopModel->getById(intval($laptopId));
            # codeguy/upload
            try {
            unlink(\Configuration::UPLOAD_DIR . $laptop->image);
            }catch(\Exception $e){
                $this->set('message', 'Greska: Greska prilikom brisanja stare slike');
            }

            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($laptopId);
            $file->addValidations([
                new \Upload\Validation\Mimetype(["image/jpeg", "image/png"]),
                new \Upload\Validation\Size("3M")
            ]);

            try {
                $file->upload();

                $fullFilename = $file->getNameWithExtension();

                $laptopModel->editById(intval($laptopId), [
                    'image' => $fullFilename
                ]);

                $this->doResize(
                    \Configuration::UPLOAD_DIR . $fullFilename,
                    \Configuration::DEFAULT_IMAGE_WIDTH,
                    \Configuration::DEFAULT_IMAGE_HEIGHT);

                return true;
            } catch (\Exception $e) {
                $this->set('message', 'Greska: ' . implode(', ', $file->getErrors()));
                return false;
            }
        }

        private function doResize(string $filePath, int $w, int $h) {
            $longer = max($w, $h);

            $image = new \Gumlet\ImageResize($filePath);
            $image->resizeToBestFit($longer, $longer);
            $image->crop($w, $h);
            $image->save($filePath);
        }
    }
