-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for laptopshop
CREATE DATABASE IF NOT EXISTS `laptopshop` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `laptopshop`;

-- Dumping structure for table laptopshop.category
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table laptopshop.category: ~2 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`, `name`) VALUES
	(1, 'Ultrabook'),
	(2, 'Hibridni'),
	(3, 'Gaming'),
	(4, 'Rugged');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table laptopshop.laptop
CREATE TABLE IF NOT EXISTS `laptop` (
  `laptop_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `os` varchar(64) NOT NULL,
  `disc_capacity` int(11) NOT NULL,
  `disc_type` varchar(64) NOT NULL,
  `diagonal` decimal(10,2) NOT NULL,
  `keyboard` varchar(64) NOT NULL,
  `numpad` tinyint(1) NOT NULL,
  `connector_type` varchar(64) NOT NULL,
  `processor` varchar(64) NOT NULL,
  `graphic_card_type` varchar(64) NOT NULL,
  `graphic_card` varchar(64) NOT NULL,
  `ram` int(11) DEFAULT NULL,
  PRIMARY KEY (`laptop_id`),
  KEY `fk1` (`category_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table laptopshop.laptop: ~16 rows (approximately)
/*!40000 ALTER TABLE `laptop` DISABLE KEYS */;
INSERT INTO `laptop` (`laptop_id`, `title`, `image`, `price`, `category_id`, `os`, `disc_capacity`, `disc_type`, `diagonal`, `keyboard`, `numpad`, `connector_type`, `processor`, `graphic_card_type`, `graphic_card`, `ram`) VALUES
	(1, 'HP Elite Dragonfly', '1.png', 1800.00, 1, 'windows 10', 2, 'SSD', 12.30, 'RS', 1, 'HDMI', 'Intel Core i7', 'integrisana', 'HDGraphics', 32),
	(2, 'ASUS ROG Zephyrus', '2.jpg', 3000.00, 1, 'Windows 10 PRO', 1, 'SSD', 15.60, 'US', 1, 'HDMI', 'Intel Core i7', 'diskretna', 'Nvidia GeForce RTX 2070', 8),
	(4, 'ACER Predator Triton 500', '4.png', 3200.00, 1, 'Windows 10 Home 64bit', 2, 'SSD', 15.60, 'EN', 1, 'HDMI', 'Intel® Core™ i7 Hexa Core Processor 9750H', 'diskretna', 'RTX 2070', 2),
	(5, 'ASUS ROG Scar III', '5.jpg', 2500.00, 3, 'Windows 10 Home 64bit', 2, 'SSD', 17.30, 'US', 1, 'HDMI', 'Intel i7-9750H', 'diskretna', 'HDGraphics', 4),
	(6, 'ASUS ROG ', '6.jpg', 2300.00, 1, 'Windows 10 Home 64bit', 1, 'SSD', 1.00, 'US', 1, 'DVI', 'Intel Core i5', 'integrisana', 'RTX 2070', 32),
	(7, 'ASUS ROG Scar III', '7.jpg', 2560.00, 1, 'Windows 10 Home 64bit', 1, 'HDD', 16.30, 'RS', 1, 'VGA', 'Intel Core i5', 'integrisana', 'HDGraphics', 4),
	(8, 'ASUS ROG ', '8.jpg', 2000.00, 1, 'Windows 10 Home 64bit', 1, 'HDD', 1.00, 'US', 1, 'DVI', 'Intel Core i5', 'diskretna', 'HDGraphics', 64),
	(9, 'ASUS ROG Scar II', '9.jpg', 1550.00, 1, 'Windows 10 Home 64bit', 1, 'SSD', 1.00, 'US', 1, 'HDMI', 'Intel Core i3', 'diskretna', 'RTX 2070', 2),
	(10, 'ASUS ROG ', '10.png', 1750.00, 1, 'Windows 10 Home 64bit', 1, 'HDD', 17.00, 'RS', 1, 'DVI', 'Intel Core i3', 'integrisana', 'HDGraphics', 64),
	(11, 'ACER Predator Triton 500', '11.png', 1350.00, 1, 'Windows 7 Ultimate', 1, 'HDD', 17.00, 'RS', 1, 'HDMI', 'Intel Core i7', 'diskretna', 'RTX 2080', 4),
	(12, 'ASUS ROG ', '12.jpg', 1200.00, 1, 'Windows 7 Ultimate', 4, 'SSD', 1.00, 'US', 1, 'HDMI', 'Intel Core i5', 'integrisana', 'HDGraphics', 2),
	(13, 'ASUS ROG ', '13.jpg', 3500.00, 1, 'Windows 7 Ultimate', 4, 'HDD', 15.60, 'RS', 1, 'VGA', 'Intel Core i3', 'diskretna', 'RTX 1080', 8),
	(14, 'ASUS ROG ', '14.jpg', 1260.00, 1, 'Windows 7 Ultimate', 1, 'SSD', 1.00, 'RS', 1, 'DVI', 'Intel Core i7', 'diskretna', 'RTX 1070', 32),
	(15, 'ASUS ROG ', '15.jpg', 1660.00, 1, 'Windows 7 Ultimate', 1, 'HDD', 1.00, 'US', 1, 'DVI', 'Intel Core i5', 'integrisana', 'HDGraphics', 8),
	(16, 'ASUS ROG ', '16.jpg', 1425.00, 1, 'Windows 7 Ultimate', 1, 'SSD', 12.50, 'RS', 1, 'HDMI', 'Intel Core i5', 'diskretna', 'RTX 1080', 16),
	(17, 'LENOVO IdeaPad L340', '17.png', 2320.00, 1, 'Windows 10 Ultimate', 1, 'HDD', 1.00, 'US', 1, 'DVI', 'Intel Core i3', 'diskretna', 'RTX 2070', 16);
/*!40000 ALTER TABLE `laptop` ENABLE KEYS */;

-- Dumping structure for table laptopshop.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table laptopshop.user: ~5 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `username`, `password`, `firstname`, `lastname`, `email`, `created_at`) VALUES
	(1, 'milan', '$2y$10$4FEgnDVpT6gnwN2tP4qAeuHkr8xetO5JxG4BJmWF0FI1BDwHIP/.S', 'Milan', 'Popovic', 'milan.popovic.13@singimail.rs', '2020-04-08 13:11:41'),
	(2, 'djura', '$2y$10$WYLoJyguuR9sK3SjSaY2/OU2.M9p1e/Pnf7LurylXqUYMrTNyf5tO', 'Djura', 'Djuric', 'djura@djuric.com', '2020-04-07 13:07:34'),
	(3, 'milutin', '$2y$10$T0Q0X6zK08QHmPON4L0JyuVwxJrDMQ1wbseym3EPlcqV16b394BZ2', 'Milutin', 'Milutinovic', 'milutin@milutinovic.com', '2020-04-07 13:10:40'),
	(4, 'pera', '$2y$10$odu3/JGiNyRaLBM6JhLiyeoQvqj18pKi97oSSeJxvL7dpYD.kYFne', 'pera', 'peric', 'pera.peric.011@singimail.rs', '2020-04-07 13:06:06'),
	(5, 'marija', '$2y$10$sZLqTAJ8bNsJL7RZNTPVlu0aLy3FNwZwIR/vyUVi5.iDgp9jfoLRW', 'Marija', 'Markovic', 'marija.markovic.09.singimail.rs', '2020-04-07 13:21:35');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
