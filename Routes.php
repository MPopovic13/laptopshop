<?php
    return [

        App\Core\Route::get('|^laptop/([0-9]+)/?$|',               'Laptop',                'show'),


        
        App\Core\Route::get('|^user/register/?$|',                  'Main',                   'getRegister'),
        App\Core\Route::post('|^user/register/?$|',                 'Main',                   'postRegister'),
        App\Core\Route::get('|^user/login/?$|',                     'Main',                   'getLogin'),
        App\Core\Route::post('|^user/login/?$|',                    'Main',                   'postLogin'),
        App\Core\Route::get('|^user/logout/?$|',                    'Main',                   'getLogout'),
        
        App\Core\Route::get('|^category/([0-9]+)/?$|',              'Category',               'show'),
        App\Core\Route::get('|^category/([0-9]+)/delete/?$|',       'Category',               'delete'),

        # User role routes:
        App\Core\Route::get('|^user/profile/?$|',                   'UserDashboard',          'index'),

        App\Core\Route::get('|^user/laptop/?$|',                'UserLaptopManagement', 'laptop'),
        App\Core\Route::get('|^user/laptop/edit/([0-9]+)/?$|',  'UserLaptopManagement', 'getEdit'),
        App\Core\Route::post('|^user/laptop/edit/([0-9]+)/?$|', 'UserLaptopManagement', 'postEdit'),
        App\Core\Route::get('|^user/laptop/add/?$|',            'UserLaptopManagement', 'getAdd'),
        App\Core\Route::post('|^user/laptop/add/?$|',           'UserLaptopManagement', 'postAdd'),
        App\Core\Route::get('|^user/laptop/delete/([0-9]+)/?$|',  'UserLaptopManagement', 'getDelete'),


        App\Core\Route::get('|^user/categories/?$|',                'UserCategoryManagement', 'categories'),
        App\Core\Route::get('|^user/categories/edit/([0-9]+)/?$|',  'UserCategoryManagement', 'getEdit'),
        App\Core\Route::post('|^user/categories/edit/([0-9]+)/?$|', 'UserCategoryManagement', 'postEdit'),
        App\Core\Route::get('|^user/categories/add/?$|',            'UserCategoryManagement', 'getAdd'),
        App\Core\Route::post('|^user/categories/add/?$|',           'UserCategoryManagement', 'postAdd'),

        App\Core\Route::get('|^user/admin/?$|',                  'UserAdminManagement',  'admin'),
        App\Core\Route::get('|^user/admin/edit/([0-9]+)/?$|',    'UserAdminManagement',  'getEdit'),
        App\Core\Route::post('|^user/admin/edit/([0-9]+)/?$|',   'UserAdminManagement',  'postEdit'),
        App\Core\Route::get('|^user/admin/add/?$|',              'UserAdminManagement',  'getAdd'),
        App\Core\Route::post('|^user/admin/add/?$|',             'UserAdminManagement',  'postAdd'),


        App\Core\Route::any('|^.*$|',                               'Main',                   'home')
    ];
