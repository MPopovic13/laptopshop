<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class UserModel extends Model {
        // Vraca niz sa svim podacima iz tabele user
        protected function getFields(): array {
            return [
                'user_id'         => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'username'        => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'password'        => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'email'           => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'firstname'       => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'lastname'        => new Field((new \App\Validators\StringValidator(0, 64)) ),    
            ];
        }

        public function getByUsername(string $username) {
            return $this->getByFieldName('username', $username);
        }
    }
