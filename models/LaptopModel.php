<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class LaptopModel extends Model {


        // Uzima sva polja iz baze
        protected function getFields(): array {
            return [
                'laptop_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'title'           => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'image'       => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'price'           => new Field((new \App\Validators\NumberValidator())->setDecimal()
                                                                        ->setUnsigned()
                                                                        ->setIntegerLength(7)
                                                                        ->setMaxDecimalDigits(2) ),
                'category_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), true),                                                        
                'os'            => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'disc_capacity'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), true),
                'disc_type'            => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'diagonal'           => new Field((new \App\Validators\NumberValidator())->setDecimal()
                                                                        ->setUnsigned()
                                                                        ->setIntegerLength(7)
                                                                        ->setMaxDecimalDigits(2) ),
                'keyboard'            => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'numpad'       => new Field((new \App\Validators\BitValidator())),
                'connector_type'            => new Field((new \App\Validators\StringValidator(0, 64)) ), 
                'processor'            => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'graphic_card_type'            => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'graphic_card'                    => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'ram'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), true)                                

            ];
        }
        // filtrira podatke iz baze za zadate parametre
        public function getAllByFilter($disc_capacity, $diagonal,$connector_type,$graphic_card_type,$ram) {
           
            $sql = 'SELECT * FROM `laptop` WHERE  `connector_type` LIKE ? AND `graphic_card_type` LIKE ? ';
            $connector_type = '%' . $connector_type . '%';
            $graphic_card_type = '%' . $graphic_card_type . '%';
            
         
            switch($disc_capacity){

                case 1:
                    $sql .= 'AND `disc_capacity` BETWEEN 0 AND 1';
                    break;
                case 2:    
                    $sql .= 'AND `disc_capacity` > 1 AND `disc_capacity` <=2 ';
                    break;
                case 3:    
                    $sql .= 'AND `disc_capacity` > 2 AND `disc_capacity` <=4 ';
                    break;
                case 4:    
                    $sql .= 'AND `disc_capacity` > 4 AND `disc_capacity` <=8 ';
                    break;
                case 5:    
                    $sql .= 'AND `disc_capacity` > 8 ';
                    break;    

            }
            
          
           switch($diagonal){
            case 1:
                $sql .= 'AND `diagonal` BETWEEN 0 AND 11';
                break;
            case 2:
                $sql .= 'AND `diagonal` > 11 AND `diagonal` <=13';
                break;
            case 3:    
                $sql .= 'AND `diagonal` > 13 AND `diagonal` <=15 ';
                break;
            case 4:    
                $sql .= 'AND `diagonal` > 15 AND `diagonal` <=17 ';
                break;
          
            case 5:    
                $sql .= 'AND `diagonal` > 17 ';
                break;    

           }

          

           switch($ram){
            case 1:
                $sql .= 'AND `ram` = 2';
                break;
            case 2:
                $sql .= 'AND `ram` = 4';
                break;
            case 3:    
                $sql .= 'AND `ram` = 8';
                break;
            case 4:    
                $sql .= 'AND `ram` = 16';
                break;
            case 5:    
                $sql .= 'AND `ram` = 32';
                break;
            case 6:    
                $sql .= 'AND `ram` >= 64 ';
                break;    

           }


           $sql .= " order by price;";
         
          



            $prep = $this->getConnection()->prepare($sql);
            if (!$prep) {
                return [];
            }

            
            $res = $prep->execute([$connector_type, $graphic_card_type]);
            if (!$res) {
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
    }